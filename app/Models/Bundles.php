<?php
/**
 * Created by PhpStorm.
 * User: teejay
 * Date: 13/01/19
 * Time: 15:22
 */

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Bundles extends Authenticatable
{
    protected $table = 'tbl_bundles';
    protected $primaryKey = 'id';

    protected $fillable = ['size'];
}