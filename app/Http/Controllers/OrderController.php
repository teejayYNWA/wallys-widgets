<?php
/**
 * Created by PhpStorm.
 * User: teejay
 * Date: 12/01/19
 * Time: 20:16
 */

namespace App\Http\Controllers;


use App\Managers\StockManager;
use Illuminate\Http\Request;

class OrderController extends Controller
{

    public function index()
    {
        return view('order.index');

    }

    public function create(Request $request, StockManager $stockManager)
    {
        // validation of user input to protect against sql injection ;-)
        $request->validate([
            'amount' => 'numeric|min:1'
        ]);

        \Session::flash('flash_message', 'Calculation complete.');

        $data = [
            'breakdown' => $stockManager->breakdown($request['amount']),
            'requested' =>$request['amount'],
        ];

        return view('.order.show', $data);

    }

}