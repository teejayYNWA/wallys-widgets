<?php
/**
 * Created by PhpStorm.
 * User: teejay
 * Date: 13/01/19
 * Time: 14:43
 */

namespace App\Managers;

class StockManager
{

    // flexible to add more packs, this would be a DB call in real life, have created migration and model for this
    private $widgetPacks = [5000, 2000, 1000, 500, 250];

    private $widgetsToShip = [];

    public function __construct()
    {
        // make sure bundles are in size order - largest first
        rsort($this->widgetPacks);
    }

    /**
     * @param int $widgetsRequested
     * @return array
     */
    public function breakdown(int $widgetsRequested = 0)
    {
        $widgetsRequired = $widgetsRequested;

        // 1. handle complete packs...
        $widgetsRequired = $this->handleCompletePacks($widgetsRequired);
        // 2. handle incomplete packs....
        $this->handleIncompletePacks($widgetsRequired);
        // 3. consolidate packs (251)...

        // 4. handle the fact we want to send the min amount of packs
        $this->handleSendingMinPacks();


        return $this->widgetsToShip;
    }

    /**
     * @param int $widgetsRequired
     * @return int
     */
    private function handleCompletePacks(int $widgetsRequired = 0)
    {
        foreach ($this->widgetPacks as $pack) {
            /**
             * find the complete number of packs this size that fit into the remaining widgets required...
             */
            $completeBundlesRequired = floor($widgetsRequired / $pack);

            if ($completeBundlesRequired >= 1) {
                for ($i = 0; $i < $completeBundlesRequired; $i++) {
                    $this->widgetsToShip[] = $pack;
                }
                $widgetsRequired = $widgetsRequired - ($completeBundlesRequired * $pack);
            }
        }

        return $widgetsRequired;
    }

    private function handleIncompletePacks(int $widgetsRequired)
    {
        /**
         * sort the bundles array so that we're only attempted to ship the smallest number of extra widgets...
         */
        sort($this->widgetPacks);

        foreach ($this->widgetPacks as $pack) {
            /**
             * if there are more widgets needed and they fit into this bundle size add it...
             */
            if ($widgetsRequired > 0 && $widgetsRequired <= $pack) {
                $this->widgetsToShip[] = $pack;
                $widgetsRequired = $widgetsRequired - $pack;
            }
        }

        return $widgetsRequired;
    }

    private function handleSendingMinPacks()
    {
        /**
         * fins any duplicates of the smallest package, check if we can send a larger pack, if so add larger remove duplicates
         */
        foreach (array_count_values($this->widgetsToShip) as $key => $value) {
            if($key == min($this->widgetPacks) && $value >1)
                {
                    if(in_array($key*$value, $this->widgetPacks) )
                        $this->widgetsToShip = array_diff($this->widgetsToShip, [$key]);
                    $this->widgetsToShip [] = $key*$value;
                 }
        }
        return $this->widgetsToShip;


    }
}