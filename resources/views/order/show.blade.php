@extends('layouts.app')

@section('content')
    <card-component header="Order Breakdown">
        @if(Session::has('flash_message'))
            <div class="alert alert-success">
                <li> {!! session('flash_message') !!}</li>
            </div>
        @endif
        To complete the order of {{$requested}} widgets, you must send the following pack(s)!
        @foreach($breakdown as $b)
                <ul>
                    <li>{{$b}}</li>
                </ul>
        @endforeach
    </card-component>
@endsection