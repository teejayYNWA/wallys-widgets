@extends('layouts.app')

@section('content')
    <card-component header="Order Calculator">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="/create" method="POST">
            @csrf
            <div class="form-group">
                <label for="name">Number of Widgets Ordered</label>
                <input type="number" class="form-control" id="amount" placeholder="Amount" name="amount">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </card-component>
@endsection